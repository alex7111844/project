# Pipeline

1 - выкачать код проекта (язык проекта не принципиально, будет плюсом если Java)

  За основу взят Java проект [Calculator](https://github.com/HouariZegai/Calculator)

2 - собрать проект в сборочном образе

3 - запустить тестирование проекта

4 - поместить собранный проект в docker-образ (не тот который для сборки а упрощенный который требуется для запуска и работы проекта)

5 - положить артефакт сборки проекта и docker образ во внешнее хранилище

  Jar артефакт в GitLab Artifacts, docker image в GitLab container registry

6 - отправить webhook во внешнюю систему о результате завершения работы

  Webhook на Slack

## Screenshots
|                Slack notification               |
:------------------------------------------------:|
 ![notification](screenshots/notification.png)

|                Artifact               |
:------------------------------------------------:|
 ![artifact](screenshots/artifact.png)

## Requirements 🔧
* Java 11 or higher.